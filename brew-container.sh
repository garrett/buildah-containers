#!/bin/bash -x

project_name=brew
real_home=/home/$USER
project_path=$real_home

container=$(buildah from --name $project_name fedora)

packages="curl file git python-setuptools which sudo"

## Get all updates and install our minimal httpd server
buildah run $container -- dnf update -y
buildah run $container -- dnf groupinstall -y 'Development Tools'
buildah run $container -- dnf install -y $packages
buildah run $container -- bash -c "echo $project_name > /etc/hostname"
buildah run $container -- bash -c "echo \"$USER ALL=(ALL) NOPASSWD: ALL\" >> /etc/sudoers"
buildah run $container -- groupadd -g $SUDO_GID $USER
buildah run $container -- useradd -ms /bin/bash -u $SUDO_UID -g $SUDO_GID $USER
buildah run $container -- sudo hostname --file /etc/hostname
buildah run $container -- sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"

## Include some buildtime annotations
buildah config --annotation "com.example.build.host=$(uname -n)" $container

## Run our server and expose the port
buildah config --user $USER $container
buildah config --workingdir /home/$USER $container
buildah config --cmd /bin/bash $container

## I'm not sure why this isn't working — for now, include it in the
## buildah run commandline
buildah config --volume $project_path:/home/$USER $container

## When this is done, run the following:
# sudo buildah run brew --hostname brew -v ~:/home
