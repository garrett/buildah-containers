#!/bin/bash -x

project_name=jekyll
real_home=/home/$USER
project_path=$real_home/Project

container=$(buildah from --name $project_name fedora)

packages="dnf-utils rubygem-bundler ruby-devel libffi-devel make gcc gcc-c++ redhat-rpm-config zlib-devel libxml2-devel libxslt-devel tar sudo v8 rubygem-jekyll-watch"
builddep="rubygems rubygem-jekyll"

## Get all updates and install our minimal httpd server
buildah run $container -- dnf update -y
buildah run $container -- dnf install -y $packages
buildah run $container -- dnf builddep -y $builddep
buildah run $container -- gem install bundler

## Run our server and expose the port
buildah config --workingdir /srv/jekyll $container
buildah config --env LANG=$LANG $container
buildah config --env TERM=$TERM $container
buildah config --env BUNDLE_SILENCE_ROOT_WARNING=true $container
buildah config --cmd "bundle exec jekyll s" $container
buildah config --port 4000 $container

## When this is done, prefix all Jekyll commands with:
# buildah run -v $PWD:/srv/jekyll jekyll
#
## Examples
#
# Use bundle set up and install:
# buildah run -v $PWD:/srv/jekyll jekyll -- bundle install
#
# Run Jekyll server:
# buildah run -v $PWD:/srv/jekyll jekyll -- bundle exec jekyll s
#
# Jekyll with hostname & incremental building (faster, but may be buggy):
# buildah run -v $PWD:/srv/jekyll jekyll -- bundle exec jekyll s -H $HOSTNAME -I
# buildah run -v $PWD:/srv/jekyll jekyll -- bundle exec jekyll s -H $HOSTNAME -I

## Tip: Add Jekyll container as an alias in ~/.bashrc or ~/.zshrc
# alias jekyll="buildah run -v $PWD:/srv/jekyll jekyll --"

## Note: You might need to set SELinux permissions with something like:
# sudo chcon -R -h -t container_file_t ~/Project
