#!/bin/bash -x

project_name=waifu2x
real_home=/home/$USER
project_path=$real_home

container=$(buildah from --name $project_name fedora)

run="buildah run $container --"

packages="dnf-utils opencv-devel clinfo beignet-devel libjpeg-devel libwebp-devel zlib-devel libtiff-devel cmake sudo git gcc gcc-c++"
#builddep="rubygems rubygem-waifu2x"

## Get all updates and install our minimal httpd server
$run dnf update -y
$run dnf install -y $packages
#$run dnf builddep -y $builddep
$run bash -c "echo $project_name > /etc/hostname"
$run bash -c "echo \"$USER ALL=(ALL) NOPASSWD: ALL\" >> /etc/sudoers"
$run groupadd -g $SUDO_GID $USER
$run useradd -ms /bin/bash -u $SUDO_UID -g $SUDO_GID $USER
$run sudo hostname --file /etc/hostname

## Include some buildtime annotations
buildah config --annotation "com.example.build.host=$(uname -n)" $container

buildah config --user $USER $container
buildah config --env LANG=$LANG $container
buildah config --env TERM=$TERM $container

$run git clone https://github.com/DeadSix27/waifu2x-converter-cpp /srv/waifu2x

buildah config --workingdir /srv/waifu2x $container

$run mkdir release
buildah config --workingdir /srv/waifu2x/release $container
$run cmake ..
$run make
$run cp ../models_rgb . -rl

buildah config --workingdir /srv/waifu2x/release/proc $container

## I'm not sure why this isn't working — for now, include it in the
## buildah run commandline
#$CONFIG --volume $project_path:/srv/waifu2x

## When this is done, run the following (in the directory):
# sudo buildah run --hostname waifu2x -v $PWD:/srv/waifu2x waifu2x

## Note: You might need to set SELinux permissions with something like:
# sudo chcon -R -h -t container_file_t ~/Project
