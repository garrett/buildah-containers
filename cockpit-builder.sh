#!/bin/bash

set -o errexit

project_name=cockpit-builder

options="--net=host"
container=$(buildah from --name $project_name fedora)

packages="dnf-utils npm findutils ack the_silver_searcher hostname sudo make git"
builddep="cockpit"

# Force NPM path
buildah config --env 'NODE_PATH=/srv/cockpit/' cockpit-builder
# Add node modules to the path
buildah config --env 'PATH=/srv/cockpit/node_modules/.bin:$PATH' cockpit-builder
# Set working dir
buildah config --workingdir /srv/cockpit $container
# Set default command
buildah config --cmd /bin/bash $container

# Get all updates and install Cockpit
buildah run $options $container -- dnf update -y
buildah run $options $container -- dnf install -y $packages
buildah run $options $container -- dnf builddep -y $builddep

# Include some buildtime annotations
buildah config --annotation "com.example.build.host=$(uname -n)" $container
buildah config --author "$(grep $USER /etc/passwd | cut -d: -f5)" $container

#####################################
######   READ THE DOCS BELOW   ######
#####################################

## When this is done, run something like the following:
# buildah run $options -v $project_path:/srv/cockpit cockpit-builder --
# (Be sure to replace $project_path with Cockpit source path)

## Note: You might need to set SELinux permissions with something like:
# sudo chcon -R -h -t container_file_t ~/Project

## Note 2: You can add this in zshrc or bashrc for a "ct" command:
# alias ct='buildah run --net=host --hostname cockpit-builder -v ~/Project/cockpit/cockpit:/srv/cockpit:z cockpit-builder --'
# (Modify the Cockpit source directory appropriately)
#
# Then, prefix any Cockpit building command with "ct" and you're
# building Cockpit in a container!
#
# You may want to run multiple commands in one shell. You can do
# something like the following (with the alias set up):
#
# ct sh -c "./autogen.sh --prefix=/opt --enable-debug && make"
# 
#
# If you symlink $project_path/dist to # ~/.local/share/cockpit your
# local Cockpit install then comes from the version of Cockpit you built
# using this container. The big advantage of this method is that you
# don't need all the development dependencies on your host system.
#
# This makes it possible to use Silverblue for development (provided you
# do an rpm-ostree overlay for cockpit-ws packages). To overlay Cockpit:
#
# rpm-ostree install cockpit-bridge cockpit-ws
#
# and then start Cockpit with:
#
# sudo systemctl enable --now cockpit.socket

echo
echo "Done building $project_name!"
echo
echo "Add the following alias to your ~/.bashrc (or ~/.zshrc, etc.):"
echo "alias ct='buildah run --net=host --hostname cockpit-builder -v ~/Projects/cockpit:/srv/cockpit:z cockpit-builder --'"
echo
echo "If you have SELinux on, you may need to do something like the following:"
echo "sudo chcon -R -h -t container_file_t ~/Projects"
echo
echo "To run multiple commands, you can do something like:"
echo 'ct sh -c "./autogen.sh --prefix=/opt --enable-debug && make"'
echo
echo "If you have missing packages, run the following before building:"
echo "ct npm install eslint clean-css uglify-js"
echo
echo "Have fun!"
echo
